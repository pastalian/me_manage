package me.manage.backend.api

import me.manage.backend.model.Nutrition
import me.manage.backend.service.StandardService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1/standard")
@CrossOrigin(origins = ["http://localhost:8080"])
class StandardController(private val standardService: StandardService) {
    @GetMapping
    fun getStandard(): Nutrition {
        return standardService.getStandard()
    }
}
