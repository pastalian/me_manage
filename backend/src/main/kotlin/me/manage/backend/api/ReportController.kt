package me.manage.backend.api

import me.manage.backend.model.Report
import me.manage.backend.service.ReportService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("api/v1/report")
@CrossOrigin(origins = ["http://localhost:8080"])
class ReportController(private val reportService: ReportService) {
    @PostMapping
    fun addReport(@RequestBody report: Report) {
        reportService.addReport(report)
    }

    @GetMapping
    fun getAllReports(): List<Report> {
        return reportService.getAllReports()
    }

    @GetMapping("week")
    fun getLastWeekReports(): List<Report> {
        return reportService.getLastWeekReports()
    }

    @GetMapping(path = ["{date}"])
    fun getReportsByDate(@DateTimeFormat(pattern = "yyyyMMdd")
                         @PathVariable("date") date: Date):
            List<Report> {
        return reportService.getReportsByDate(date)
    }
}
