package me.manage.backend.service

import me.manage.backend.dao.FoodDao
import me.manage.backend.model.Food
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

@Service
class FoodService(@Qualifier("food_postgres")
                  private val foodDao: FoodDao) {
    private var foodList = foodDao.findAllFood()

    fun addFood(food: Food) {
        foodDao.addFood(food)
        foodList = foodDao.findAllFood()
    }

    fun getFoodList() = foodList

    fun findFoodByName(name: String) = foodDao.findFoodByName(name)
}
