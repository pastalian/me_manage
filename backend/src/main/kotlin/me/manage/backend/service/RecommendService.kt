package me.manage.backend.service

import me.manage.backend.dao.FoodDao
import me.manage.backend.dao.ReportDao
import me.manage.backend.dao.StandardDao
import me.manage.backend.model.Food
import me.manage.backend.model.Nutrition
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import java.util.*

@Service
class RecommendService(@Qualifier("report_postgres")
                       private val reportDao: ReportDao,
                       @Qualifier("food_postgres")
                       private val foodDao: FoodDao,
                       @Qualifier("fake")
                       private val standardDao: StandardDao) {

    private val std = standardDao.getStandard()

    fun getRecOrderFoodList(nutrition: Nutrition): List<Food> {
        val foodList = foodDao.findAllFood()
        val co = calcCoefficient()
        return foodList.sortedWith(Comparator { p0, p1 ->
            val l = (nutrition + p0.nutrition).evaluate(std, co)
            val r = (nutrition + p1.nutrition).evaluate(std, co)
            if (l > r) 1 else if (l == r) 0 else -1
        })
    }

    fun getRecWithinBudget(budget: Int, nutrition: Nutrition): List<Food> {
        val foodList = foodDao.findAllFood()
        val co = calcCoefficient()
        val dp = Array(foodList.size + 1) { Array(budget + 1) { nutrition } }
        for ((i, f) in foodList.withIndex()) {
            for (j in 1..budget) {
                if (j >= f.cost) {
                    val n = dp[i][j - f.cost] + f.nutrition

                    if (n.evaluate(std, co) < dp[i][j].evaluate(std, co)) {
                        dp[i + 1][j] = n
                    } else {
                        dp[i + 1][j] = dp[i][j]
                    }
                } else {
                    dp[i + 1][j] = dp[i][j]
                }
            }
        }
        return backTraceDp(dp, budget, foodList)
    }

    private fun backTraceDp(dp: Array<Array<Nutrition>>, budget: Int,
                            foodList: List<Food>):
            List<Food> {
        val ret = mutableListOf<Food>()
        var cur = budget
        for (i in foodList.size downTo 1) {
            if (cur < foodList[i - 1].cost) continue
            if (dp[i][cur].same(dp[i - 1][cur - foodList[i - 1].cost] +
                            foodList[i - 1].nutrition)) {
                ret.add(foodList[i - 1])
                cur -= foodList[i - 1].cost
            }
        }
        return ret
    }

    private fun calcCoefficient(): Nutrition {
        val ret = Nutrition(1F, 1F, 1F, 1F, 0.5F)
        val diff = calcWeekPercentage()
        if (diff.energy < 1) {
            ret.energy = 2 - diff.energy
        }
        if (diff.protein < 1) {
            ret.protein = 2 - diff.protein
        }
        if (diff.lipid < 1) {
            ret.lipid = 2 - diff.lipid
        }
        if (diff.carbohydrate < 1) {
            ret.carbohydrate = 2 - diff.carbohydrate
        }
        return ret
    }

    private fun calcWeekPercentage(): Nutrition {
        val week = reportDao.selectLastWeekReports()

        val cnt = mutableSetOf<Date>()
        var sum = Nutrition(0F, 0F, 0F, 0F, 0F)
        week.forEach {
            sum += it.getNutrition()
            cnt.add(it.getDate())
        }

        return (sum / cnt.size) / std
    }
}
