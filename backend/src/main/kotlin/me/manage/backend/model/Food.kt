package me.manage.backend.model

class Food(
        val id: Int?,
        val name: String,
        val cost: Int,
        val nutrition: Nutrition) {
}
