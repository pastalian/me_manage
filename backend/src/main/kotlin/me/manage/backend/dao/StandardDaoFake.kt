package me.manage.backend.dao

import me.manage.backend.model.Nutrition
import org.springframework.stereotype.Repository

@Repository("fake")
class StandardDaoFake() : StandardDao {
    override fun getStandard(): Nutrition {
        return Nutrition(
                2650F,
                // protein(g) = energy(kcal) * protein(%e) / 4 / 100
                132.5F,
                // lipid(g) = energy(kcal) * lipid(%e) / 9 / 100
                88.3F,
                // carbon(g) = energy(kcal) * carbon(%e) / 4 / 100
                430.6F,
                // na(mg)
                3150F
        )
    }
}
