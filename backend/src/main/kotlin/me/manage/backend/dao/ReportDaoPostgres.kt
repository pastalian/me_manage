package me.manage.backend.dao

import me.manage.backend.model.Nutrition
import me.manage.backend.model.Report
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.util.*

@Repository("report_postgres")
class ReportDaoPostgres(private val jdbcTemplate: NamedParameterJdbcTemplate)
    : ReportDao {
    private val rowMapper = RowMapper<Report> { rs, _ ->
        Report(
                Nutrition(rs.getFloat("energy"),
                        rs.getFloat("protein"),
                        rs.getFloat("lipid"),
                        rs.getFloat("carbohydrate"),
                        rs.getFloat("sodium")),
                rs.getString("name"),
                rs.getDate("ate_at"))
    }

    override fun addReport(report: Report) {
        val sql = "INSERT INTO report (energy, protein, lipid, carbohydrate, " +
                "sodium, name, ate_at) VALUES (:e, :p, :l, :c, :s, :n, :a)"
        val nutrition = report.getNutrition()
        val params = MapSqlParameterSource()
                .addValue("e", nutrition.energy)
                .addValue("p", nutrition.protein)
                .addValue("l", nutrition.lipid)
                .addValue("c", nutrition.carbohydrate)
                .addValue("s", nutrition.sodium)
                .addValue("n", report.getName())
                .addValue("a", report.getDate())
        jdbcTemplate.update(sql, params)
    }

    override fun selectAllReports(): List<Report> {
        val sql = "SELECT * FROM report"
        return jdbcTemplate.query(sql, rowMapper)
    }

    override fun selectLastWeekReports(): List<Report> {
        val sql = "SELECT * FROM report " +
                "WHERE ate_at > now() - interval '1 week'"
        return jdbcTemplate.query(sql, rowMapper)
    }

    override fun selectReportsByDate(ate_at: Date): List<Report> {
        val sql = "SELECT * FROM report WHERE ate_at = :a"
        return jdbcTemplate.query(sql,
                MapSqlParameterSource().addValue("a", ate_at),
                rowMapper)
    }
}
