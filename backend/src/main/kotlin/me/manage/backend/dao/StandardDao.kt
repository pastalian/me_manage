package me.manage.backend.dao

import me.manage.backend.model.Nutrition

interface StandardDao {
    fun getStandard(): Nutrition
}
