CREATE TABLE food
(
    id           SERIAL PRIMARY KEY,
    name         TEXT      NOT NULL,
    cost         INTEGER   NOT NULL,
    energy       DECIMAL   NOT NULL,
    protein      DECIMAL   NOT NULL,
    lipid        DECIMAL   NOT NULL,
    carbohydrate DECIMAL   NOT NULL,
    sodium       DECIMAL   NOT NULL,
    created_at   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
