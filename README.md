# me-manage
simple nutrition management application.

## Build
```
docker-compose build .
```

## Run
```
docker-compose up frontend
```

## Screenshots
![home](/uploads/75f33b79e082deabda2dbdce37154db1/19-11-30_171719.png)
![stats](/uploads/7553322164e2872c16156337107ba79f/19-11-30_171622.png)
![recommend](/uploads/2e026c23f6d417598e5c9771dd2a4d16/19-11-30_171750.png)
